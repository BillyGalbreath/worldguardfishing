package net.pl3x.bukkit.worldguard.listener;

import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import net.pl3x.bukkit.worldguard.Logger;
import net.pl3x.bukkit.worldguard.configuration.Config;
import net.pl3x.bukkit.worldguard.configuration.Lang;
import net.pl3x.bukkit.worldguard.flag.FishingFlag;
import net.pl3x.bukkit.worldguard.flag.ReelEntityFlag;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;

public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerFish(PlayerFishEvent event) {
        Entity player = event.getPlayer();
        Entity caught = event.getCaught();
        Entity hook = event.getHook();

        ApplicableRegionSet caughtRegionSet = (caught == null) ? null : getApplicableRegions(caught.getLocation());

        if (event.getState() == PlayerFishEvent.State.CAUGHT_ENTITY &&
                caught != null && ReelEntityFlag.getFlag(caughtRegionSet) == StateFlag.State.DENY) {
            player.sendMessage(Lang.REELING_ENTITY_DENIED.toString());
            Logger.debug("Reeling entity denied at " + caught.getLocation());
            event.setCancelled(true);
            if (Config.REMOVE_HOOK.getBoolean()) {
                hook.remove();
            }
            return;
        }

        boolean deny = false;
        if (FishingFlag.getFlag(getApplicableRegions(player.getLocation())) == StateFlag.State.DENY) {
            Logger.debug("Fishing denied at " + player.getLocation());
            deny = true;
        } else if (caught != null && FishingFlag.getFlag(caughtRegionSet) == StateFlag.State.DENY) {
            Logger.debug("Fishing denied at " + caught.getLocation());
            deny = true;
        } else if (FishingFlag.getFlag(getApplicableRegions(hook.getLocation())) == StateFlag.State.DENY) {
            Logger.debug("Fishing denied at " + hook.getLocation());
            deny = true;
        }

        if (deny) {
            player.sendMessage(Lang.FISHING_DENIED.toString());
            event.setCancelled(true);
            if (Config.REMOVE_HOOK.getBoolean()) {
                hook.remove();
            }
        }
    }

    private ApplicableRegionSet getApplicableRegions(Location location) {
        RegionManager regionManager = WorldGuardPlugin.getPlugin(WorldGuardPlugin.class).getGlobalRegionManager().get(location.getWorld());
        if (regionManager == null) {
            return null;
        }
        return regionManager.getApplicableRegions(BukkitUtil.toVector(location));
    }
}
