package net.pl3x.bukkit.worldguard.flag;


import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FishingFlag extends StateFlag {
    public static final FishingFlag flag = new FishingFlag();

    public FishingFlag() {
        super("fishing", true);
    }

    public static State getFlag(ApplicableRegionSet set) {
        return set.getFlag(flag);
    }

    public static State getFlag(ProtectedRegion region) {
        return region.getFlag(flag);
    }

    private static List elements() {
        List elements = new ArrayList(Arrays.asList(DefaultFlag.getFlags()));
        elements.add(flag);
        return elements;
    }

    public static void injectHax() {
        try {
            Field flagsListField = DefaultFlag.class.getDeclaredField("flagsList");
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(flagsListField, flagsListField.getModifiers() & 0xFFFFFFEF);
            flagsListField.setAccessible(true);
            List elements = elements();
            Flag[] list = new Flag[elements.size()];
            for (int i = 0; i < elements.size(); i++) {
                list[i] = ((Flag) elements.get(i));
            }
            flagsListField.set(null, list);

            WorldGuardPlugin worldguard = WorldGuardPlugin.getPlugin(WorldGuardPlugin.class);
            for (RegionManager rm : worldguard.getGlobalRegionManager().getLoaded()) {
                rm.load();
            }

            try {
                FlagRegistry flagRegistry = worldguard.getFlagRegistry();
                Field initializedField = flagRegistry.getClass().getDeclaredField("initialized");
                initializedField.setAccessible(true);
                initializedField.setBoolean(flagRegistry, false);
                flagRegistry.register(flag);
                initializedField.setBoolean(flagRegistry, true);
            } catch (Exception ignore) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
