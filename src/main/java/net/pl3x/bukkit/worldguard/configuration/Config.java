package net.pl3x.bukkit.worldguard.configuration;

import net.pl3x.bukkit.worldguard.WorldGuardFishing;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml"),
    REMOVE_HOOK(true);

    private final WorldGuardFishing plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = WorldGuardFishing.getPlugin();
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }
}
