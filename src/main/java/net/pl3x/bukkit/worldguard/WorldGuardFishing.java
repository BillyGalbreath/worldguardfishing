package net.pl3x.bukkit.worldguard;

import net.pl3x.bukkit.worldguard.configuration.Lang;
import net.pl3x.bukkit.worldguard.flag.FishingFlag;
import net.pl3x.bukkit.worldguard.flag.ReelEntityFlag;
import net.pl3x.bukkit.worldguard.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class WorldGuardFishing extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        PluginManager pm = Bukkit.getPluginManager();
        if (!pm.isPluginEnabled("WorldGuard")) {
            Logger.error("WorldGuard not installed/found! Disabling plugin.");
            pm.disablePlugin(this);
            return;
        }

        ReelEntityFlag.injectHax();
        FishingFlag.injectHax();

        pm.registerEvents(new PlayerListener(), this);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static WorldGuardFishing getPlugin() {
        return WorldGuardFishing.getPlugin(WorldGuardFishing.class);
    }
}
